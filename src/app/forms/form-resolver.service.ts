import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Form } from '../shared/form.model';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { FormService } from '../shared/form.service';

@Injectable({
  providedIn: 'root'
})
export class FormResolverService implements Resolve<Form>{

  constructor(private router: Router, private formService: FormService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Form> | Observable<never> {
    const formId = <string>route.params['id'];

    return this.formService.fetchForm(formId).pipe(mergeMap(form => {
      if(form) {
        return of(form);
      }

      void this.router.navigate(['']);
      return EMPTY;
    }));
  }
}
