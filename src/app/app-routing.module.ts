import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SuccessfulRegistrationComponent } from './successful-registration.component';
import { FormsComponent } from './forms/forms.component';
import { MainFormComponent } from './main-form/main-form.component';
import { FormDetailsComponent } from './forms/form-details/form-details.component';
import { FormResolverService } from './forms/form-resolver.service';

const routes: Routes = [
  {path: '', component: FormsComponent},
  {path: 'forms', component: FormsComponent, children: [
      {path: 'add-form', component: MainFormComponent},
      {path: ':id', component: FormDetailsComponent, resolve: {
          form: FormResolverService
        }
      }
    ]},
  {path: 'success', component: SuccessfulRegistrationComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
