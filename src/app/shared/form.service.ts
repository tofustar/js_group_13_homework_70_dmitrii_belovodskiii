import { Injectable } from '@angular/core';
import { Form } from './form.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable()

export class FormService {
  formsChange = new Subject<Form[]>();
  private forms: Form[] = [];

  constructor(private http: HttpClient) {}

  fetchForms() {
    return this.http.get<{[key:string]: Form}>('https://js-group-13-default-rtdb.firebaseio.com/reactive-form.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const formData = result[id];
          return new Form(id, formData.name, formData.surname, formData.patronymic, formData.phone, formData.job, formData.selectType, formData.size, formData.comment, formData.skill)
        });
      }))
      .subscribe(forms => {
        this.forms = forms;
        this.formsChange.next(this.forms.slice());
      });
  }

  fetchForm(id: string) {
    return this.http.get<Form | null>(`https://js-group-13-default-rtdb.firebaseio.com/reactive-form/${id}.json`)
      .pipe(map(result => {
        if(!result) {
          return null;
        }
        return new Form(id, result.name, result.surname, result.patronymic, result.phone, result.job, result.selectType, result.size, result.comment, result.skill)
      }))
  }

  addForm(form: Form) {
    return this.http.post('https://js-group-13-default-rtdb.firebaseio.com/reactive-form.json', form).pipe();
  }

}
