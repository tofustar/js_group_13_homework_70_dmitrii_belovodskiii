export class Form {
  constructor(
    public id: string,
    public name: string,
    public surname: string,
    public patronymic: string,
    public phone: string,
    public job: string,
    public selectType: string,
    public size: string,
    public comment: string,
    public skill: [{
      key:string
    }]
  ) {}
}
