import { Component } from '@angular/core';

@Component({
  selector: 'app-successful-registration',
  template: `<h2>Registration was successful</h2>`,
  styles: []
})

export class SuccessfulRegistrationComponent {}
