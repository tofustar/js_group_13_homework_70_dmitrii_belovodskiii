import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { phoneValidator } from './validate-phone.directive';
import { Router } from '@angular/router';
import { FormService } from '../shared/form.service';

@Component({
  selector: 'app-main-form',
  templateUrl: './main-form.component.html',
  styleUrls: ['./main-form.component.css']
})
export class MainFormComponent implements OnInit {
  mainForm!: FormGroup;

  constructor(private router: Router, private formService: FormService) {}


  ngOnInit(): void {
    this.mainForm = new FormGroup({
      name: new FormControl('', Validators.required),
      surname: new FormControl('', Validators.required),
      patronymic: new FormControl('', Validators.required),
      phone: new FormControl('', [
        Validators.required,
        phoneValidator,
      ]),
      job: new FormControl('', Validators.required),
      selectType: new FormControl('', Validators.required),
      size: new FormControl('', Validators.required),
      comment: new FormControl('', [
        Validators.required,
        Validators.maxLength(300),
      ]),
      skills: new FormArray([])
    });
  }

  saveForm() {
    this.formService.addForm(this.mainForm.value).subscribe(() => {
      void this.router.navigate(['success']);
    });
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.mainForm.get(fieldName);
    return field && field.touched && field.errors?.[errorType];
  }

  addSkill() {
    const skills = <FormArray>this.mainForm.get('skills');
    const skillGroup = new FormGroup({
      skill: new FormControl('', Validators.required),
      level: new FormControl('', Validators.required)
    })
    skills.push(skillGroup);
  }

  getSkillControls() {
    const skills = <FormArray>this.mainForm.get('skills');
    return skills.controls;
  }
}
