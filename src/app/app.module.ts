import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainFormComponent } from './main-form/main-form.component';
import { FormsComponent } from './forms/forms.component';
import { FormDetailsComponent } from './forms/form-details/form-details.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ValidatePhoneDirective } from './main-form/validate-phone.directive';
import { FormService } from './shared/form.service';
import { HttpClientModule } from '@angular/common/http';
import { SuccessfulRegistrationComponent } from './successful-registration.component';

@NgModule({
  declarations: [
    AppComponent,
    MainFormComponent,
    FormsComponent,
    FormDetailsComponent,
    ToolbarComponent,
    ValidatePhoneDirective,
    SuccessfulRegistrationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [FormService],
  bootstrap: [AppComponent]
})
export class AppModule { }
